import React, {useState, useEffect, useContext, useRef} from 'react';
import {Link} from 'react-router-dom';

import {If, Then, Else, When} from 'react-if';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import ListGroup from 'react-bootstrap/ListGroup';
import Accordion from 'react-bootstrap/Accordion';

import {Context} from './Provider';
import Shipping from './Shipping';
import Maintenance from './Maintenance';

const {localStorage} = window;
const {NODE_ENV} = process.env;

class _DataTransfer {
  constructor() {
    return new ClipboardEvent('').clipboardData || new DataTransfer();
  }
}

const defaultStatus = '';
const defaultStatusVariant = 'info';

function BadgeRequest() {
  const [status, setStatus] = useState(defaultStatus);
  const [statusVariant, setStatusVariant] = useState(defaultStatusVariant);
  const [printerDetails, setPrinterDetails] = useState({});
  const {badge} = useContext(Context);
  const fileEl = useRef(null);

  const now = new Date();
  const maintenance =
    (NODE_ENV !== 'development' &&
      (now.getUTCDay() === 6 && now.getUTCHours() >= 18)) ||
    (now.getUTCDay() === 0 && now.getUTCHours() < 6);

  useEffect(() => {
    const {current} = fileEl;
    if (current && badge) {
      const file = new File([badge], 'fromtemplate.png', {type: badge.type});
      // https://gist.github.com/guest271314/7eac2c21911f5e40f48933ac78e518bd
      const dt = new _DataTransfer();
      dt.items.add(file);
      if (dt.files.length) {
        current.files = dt.files; // set `FileList` of `dt.files`: `DataTransfer.files` to `input.files`
      }
      current.onchange = e => {
        console.log(e, current.files);
      };
    }
  }, [badge, fileEl]);

  useEffect(() => {
    async function checkStatus() {
      try {
        const resp = await fetch('/.netlify/functions/status');
        if (resp.ok) {
          setStatus(defaultStatus);
          setPrinterDetails(await resp.json());
        } else {
          setStatusVariant('danger');
          setStatus('OFFLINE');
        }
      } catch (e) {
        setStatusVariant('danger');
        setStatus('OFFLINE');
      }
    }
    checkStatus();
  }, []);

  const handleSubmit = async event => {
    event.preventDefault();
    setStatus('Submitting...');
    const {target} = event;
    const formData = new FormData(target);

    try {
      const response = await fetch('/', {
        method: 'POST',
        body: formData,
      });
      if (response.ok) {
        const object = {};
        formData.forEach((value, key) => (object[key] = value));
        localStorage.setItem('form', JSON.stringify(object));
        setTimeout(() => {
          setStatusVariant(defaultStatusVariant);
          setStatus(defaultStatus);
        }, 3000);
        setStatusVariant('success');
        setStatus('Form Submission Successful!!');
      } else {
        setStatusVariant('warning');
        setStatus('Form Submission Failed!');
      }
    } catch (e) {
      console.log(e);
      setStatusVariant('warning');
      setStatus('Form Submission Failed!');
    }
  };

  const previousJSON = localStorage.getItem('form');
  let previous = {};
  if (previousJSON) {
    previous = JSON.parse(previousJSON);
  }

  return (
    <>
      <If condition={maintenance}>
        <Then>
          <Maintenance />
        </Then>
        <Else>
          <Row>
            <Col>
              <When condition={status !== ''}>
                <Alert variant={statusVariant}>{status}</Alert>
              </When>
            </Col>
          </Row>
          <Form data-netlify="true" onSubmit={handleSubmit}>
            <input type="hidden" name="form-name" value="request" />
            <Form.Group as={Row} controlId="formHorizontalBotField" hidden>
              <Form.Label column sm={2}>
                Don’t fill this out if you’re human:
              </Form.Label>
              <Col sm={6}>
                <Form.Control name="bot-field" />
              </Col>
            </Form.Group>

            <Row>
              <Col md={12} lg={6}>
                <Card>
                  <Card.Header>Destination</Card.Header>
                  <Card.Body>
                    <Form.Group as={Row} controlId="formHorizontalName">
                      <Form.Label required column sm={2}>
                        Name
                      </Form.Label>
                      <Col sm={6}>
                        <Form.Control
                          name="name"
                          type="text"
                          placeholder="Name for address label"
                          defaultValue={previous.name}
                        />
                      </Col>
                    </Form.Group>

                    <Form.Group controlId="formGridAddress1">
                      <Form.Label>Address</Form.Label>
                      <Form.Control
                        required
                        type="text"
                        name="address1"
                        placeholder="1234 Main St"
                        defaultValue={previous.address1}
                      />
                    </Form.Group>

                    <Form.Group controlId="formGridAddress2">
                      <Form.Label>Address 2</Form.Label>
                      <Form.Control
                        name="address2"
                        type="text"
                        placeholder="Apartment, studio, or floor"
                        defaultValue={previous.address2}
                      />
                    </Form.Group>

                    <Form.Row>
                      <Form.Group as={Col} md="4" controlId="formGridCity">
                        <Form.Label>City</Form.Label>
                        <Form.Control
                          required
                          type="text"
                          name="city"
                          defaultValue={previous.city}
                        />
                      </Form.Group>

                      <Form.Group as={Col} md="5" controlId="formGridRegion">
                        <Form.Label>State / Province / Region</Form.Label>
                        <Form.Control
                          type="text"
                          name="region"
                          defaultValue={previous.region}
                        />
                      </Form.Group>

                      <Form.Group as={Col} md="3" controlId="formGridPostal">
                        <Form.Label>Postal Code</Form.Label>
                        <Form.Control
                          required
                          type="text"
                          name="postalcode"
                          defaultValue={previous.postalcode}
                        />
                      </Form.Group>
                    </Form.Row>

                    <Form.Group as={Row} controlId="formGridCountry">
                      <Form.Label column sm={3}>
                        Country
                      </Form.Label>
                      <Col sm={5}>
                        <Form.Control
                          type="text"
                          name="country"
                          defaultValue={previous.country || 'USA'}
                        />
                      </Col>
                    </Form.Group>
                  </Card.Body>
                </Card>
              </Col>

              <Col md={12} lg={6}>
                <Card>
                  <Card.Header>Badge</Card.Header>
                  <Card.Body>
                    <Form.Group as={Row} controlId="formHorizontalGallery">
                      <Col sm={6}>
                        <Form.Switch
                          name="gallery"
                          label={
                            <span>
                              Include image in{' '}
                              <Link to="/gallery">gallery</Link>
                            </span>
                          }
                        />
                      </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formHorizontalEmail">
                      <Form.Label column sm={2}>
                        Email
                      </Form.Label>
                      <Col sm={6}>
                        <Form.Control
                          required
                          name="email"
                          type="email"
                          defaultValue={previous.email}
                        />
                      </Col>
                      <Col sm={4}>
                        <Form.Text id="emailHelpBlock">
                          To prevent fake submissions. Must be 'high' or
                          'medium' reputation on{' '}
                          <a href="https://emailrep.io/">
                            https://emailrep.io/
                          </a>
                          .
                        </Form.Text>
                      </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formGridNotes">
                      <Form.Label column sm={2}>
                        Notes
                      </Form.Label>
                      <Col sm={6}>
                        <Form.Control
                          as="textarea"
                          name="notes"
                          defaultValue={previous.notes}
                        />
                      </Col>
                      <Col sm={4}>
                        <Form.Text id="notesHelpBlock">
                          Anything else: appreciation, details of pre-arranged
                          requests, feature suggestions, etc.
                        </Form.Text>
                      </Col>
                    </Form.Group>
                    <Form.Group as={Row}>
                      <Col md={12} lg={6}>
                        <Form.File
                          ref={fileEl}
                          custom
                          required
                          name="badge"
                          label="image file"
                        />
                      </Col>
                      <Col
                        md={12}
                        lg={6}
                        className="text-center justify-content-center">
                        <Button
                          type="submit"
                          disabled={status !== defaultStatus}>
                          Request
                        </Button>
                      </Col>
                    </Form.Group>
                    <Form.Text>
                      <ul>
                        <li>1MB limit</li>
                        <li>png/jpg only</li>
                        <li>Aim for 1012px * 638px</li>
                        <li>
                          PVC CR80, contact me for special requests (magstripe,
                          RFID, etc)
                        </li>
                      </ul>
                    </Form.Text>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Form>
          <Row>
            <Col>
              <If condition={status !== ''}>
                <Then>
                  <Alert variant={statusVariant}>{status}</Alert>
                </Then>
              </If>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <Shipping />
            </Col>
          </Row>
          <Row className="mt-5">
            <Col>
              <Accordion>
                <Card>
                  <Accordion.Toggle as={Card.Header} eventKey="0">
                    Printer Status
                  </Accordion.Toggle>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body>
                      <When condition={Object.keys(printerDetails).length > 0}>
                        <ListGroup variant="flush">
                          <ListGroup.Item
                            variant={
                              printerDetails.state === 'idle' ? '' : 'warning'
                            }>
                            State: {printerDetails.state}
                          </ListGroup.Item>
                          <When condition={printerDetails.message !== ''}>
                            <ListGroup.Item variant="info">
                              Message: {printerDetails.message}
                            </ListGroup.Item>
                          </When>
                          <When condition={printerDetails.reasons !== 'none'}>
                            <ListGroup.Item variant="info">
                              Reasons: {printerDetails.reasons}
                            </ListGroup.Item>
                          </When>
                        </ListGroup>
                      </When>
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
            </Col>
          </Row>
        </Else>
      </If>
    </>
  );
}

export default BadgeRequest;
