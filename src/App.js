import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from 'react-router-dom';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';

import Provider from './Provider';
import BadgeRequest from './BadgeRequest';
import Viewer from './Viewer';
import Designer from './Designer';
import Footer from './Footer';
import Gallery from './Gallery';

import './App.css';

const activeClassName = 'active';

function App() {
  return (
    <Router>
      <Navbar collapseOnSelect expand="sm" bg="light" variant="light">
        <Navbar.Brand as={NavLink} to="/" activeClassName={activeClassName}>
          Badges of
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="https://gitlab.com/bettse/badgeme/">
              Sourcecode (GitLab)
            </Nav.Link>
            <Nav.Link as={NavLink} to="/view" activeClassName={activeClassName}>
              View
            </Nav.Link>
            <Nav.Link
              as={NavLink}
              to="/design"
              activeClassName={activeClassName}>
              Design
            </Nav.Link>
            <Nav.Link
              as={NavLink}
              to="/gallery"
              activeClassName={activeClassName}>
              Gallery
            </Nav.Link>
          </Nav>
          <Nav>
            <Navbar.Text>
              <img
                src="https://api.netlify.com/api/v1/badges/2866ca28-537d-41a7-991b-6a246e7fff5c/deploy-status"
                alt="Netlify Status"
              />
            </Navbar.Text>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Container fluid="md" className="mt-3">
        <Provider>
          <Switch>
            <Route path="/view">
              <Viewer />
            </Route>
            <Route path="/template">
              <Viewer />
            </Route>
            <Route path="/design">
              <Designer />
            </Route>
            <Route path="/gallery">
              <Gallery />
            </Route>
            <Route path="/">
              <BadgeRequest />
            </Route>
          </Switch>
        </Provider>
      </Container>
      <Footer />
    </Router>
  );
}

export default App;
