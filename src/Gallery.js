import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';

import {If, Then, Else, When} from 'react-if';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import CardColumns from 'react-bootstrap/CardColumns';
import Alert from 'react-bootstrap/Alert';
import Image from 'react-bootstrap/Image';
import Modal from 'react-bootstrap/Modal';
import Spinner from 'react-bootstrap/Spinner';
import Accordion from 'react-bootstrap/Accordion';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

function Gallery() {
  const [status, setStatus] = useState('Loading...');
  const [statusVariant, setStatusVariant] = useState('info');
  const [badges, setBadges] = useState([]);
  const [selected, setSelected] = useState(null);
  const [templates, setTemplates] = useState([]);

  useEffect(() => {
    async function loadBadges() {
      try {
        const resp = await fetch('/.netlify/functions/gallery');
        if (resp.ok) {
          const json = await resp.json();
          const {badges} = json;
          setBadges(badges);
          setStatus('');
        } else {
          setStatusVariant('danger');
          setStatus('OFFLINE');
        }
      } catch (e) {
        setStatusVariant('danger');
        setStatus('OFFLINE');
      }
    }
    loadBadges();
  }, []);

  useEffect(() => {
    async function loadTemplates() {
      try {
        const resp = await fetch('/.netlify/functions/templates');
        if (resp.ok) {
          const json = await resp.json();
          const {templates} = json;
          setTemplates(templates);
          setStatus('');
        } else {
          setStatusVariant('danger');
          setStatus('OFFLINE');
        }
      } catch (e) {
        setStatusVariant('danger');
        setStatus('OFFLINE');
      }
    }
    loadTemplates();
  }, []);

  const handleSubmit = async event => {
    event.preventDefault();
    setStatus('Submitting...');
    const {target} = event;
    const formData = new FormData(target);

    try {
      const response = await fetch('/', {
        method: 'POST',
        body: formData,
      });
      if (response.ok) {
        const object = {};
        formData.forEach((value, key) => (object[key] = value));
        localStorage.setItem('form', JSON.stringify(object));
        setTimeout(() => {
          setStatusVariant('');
          setStatus('');
        }, 3000);
        setStatusVariant('success');
        setStatus('Form Submission Successful!!');
      } else {
        setStatusVariant('warning');
        setStatus('Form Submission Failed!');
      }
    } catch (e) {
      console.log(e);
      setStatusVariant('warning');
      setStatus('Form Submission Failed!');
    }
  };

  const handleClose = () => setSelected(null);

  const selectedBadge = badges[selected] || {};

  return (
    <>
      <Row>
        <Col>
          <When condition={status !== ''}>
            <Alert variant={statusVariant} className="text-center">
              <When condition={status === 'Loading...'}>
                <Spinner animation="grow" role="status" />
              </When>
              {status}
              <When condition={status === 'Loading...'}>
                <Spinner animation="grow" role="status" />
              </When>
            </Alert>
          </When>
        </Col>
      </Row>
      <Row>
        <Col>
          <h3>Badges</h3>
          <p>
            These are badges that have been printed, the submitter chose to
            include them in the gallery.
          </p>

          <If condition={badges.length === 0}>
            <Then>
              <Alert variant="secondary" className="text-center">
                No badges to display. Why not submit one?
              </Alert>
            </Then>
            <Else>
              <CardColumns>
                {badges.map((badge, i) => {
                  const {filename, url} = badge;
                  return (
                    <Card key={i} border="secondary" className="text-center">
                      <Card.Body>
                        <Card.Title>{filename}</Card.Title>
                        <Card.Img
                          src={url}
                          onClick={() => setSelected(i)}
                          style={{width: '50%'}}
                        />
                      </Card.Body>
                    </Card>
                  );
                })}
              </CardColumns>
            </Else>
          </If>
        </Col>
      </Row>
      <Row>
        <Col>
          <h3>Templates</h3>
          <p>
            You can download these and use the &nbsp;
            <Link to="/view">Viewer</Link>&nbsp; to customize
          </p>
          <If condition={templates.length === 0}>
            <Then>
              <Alert variant="secondary" className="text-center">
                No templates to display. Why not submit one?
              </Alert>
            </Then>
            <Else>
              <CardColumns>
                {templates.map((template, i) => {
                  const {name, url} = template;
                  return (
                    <Card
                      key={i}
                      border="secondary"
                      className="text-center"
                      style={{width: '50%'}}>
                      <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <a href={url}>
                          <Card.Img src={url} />
                        </a>
                      </Card.Body>
                    </Card>
                  );
                })}
              </CardColumns>
            </Else>
          </If>
          <Accordion>
            <Card>
              <Accordion.Toggle as={Card.Header} eventKey="0">
                Submit Template
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="0">
                <Card.Body>
                  <Form data-netlify="true" onSubmit={handleSubmit}>
                    <input
                      type="hidden"
                      name="form-name"
                      value="submitTemplate"
                    />
                    <Form.Group
                      as={Row}
                      controlId="formHorizontalBotField"
                      hidden>
                      <Form.Label column sm={2}>
                        Don’t fill this out if you’re human:
                      </Form.Label>
                      <Col sm={6}>
                        <Form.Control name="bot-field" />
                      </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formHorizontalName">
                      <Form.Label required column sm={2}>
                        Name
                      </Form.Label>
                      <Col sm={6}>
                        <Form.Control name="name" type="text" />
                      </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formHorizontalSecret">
                      <Form.Label required column sm={2}>
                        Secret
                      </Form.Label>
                      <Col sm={6}>
                        <Form.Control name="secret" type="text" />
                      </Col>
                      <Form.Text id="secretHelpBlock">
                        Secret will be used later for updating/deleting
                      </Form.Text>
                    </Form.Group>
                    <Form.Group as={Row}>
                      <Col md={12} lg={6}>
                        <Form.File
                          custom
                          required
                          name="template"
                          label="Template (png with embedded json)"
                        />
                      </Col>
                      <Col
                        md={12}
                        lg={6}
                        className="text-center justify-content-center">
                        <Button type="submit" disabled={status !== ''}>
                          Submit
                        </Button>
                      </Col>
                    </Form.Group>
                  </Form>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </Col>
      </Row>
      <Modal centered size="lg" show={selected !== null} onHide={handleClose}>
        <Modal.Header closeButton>{selectedBadge.filename}</Modal.Header>
        <Modal.Body>
          <Image fluid src={selectedBadge.url} />
        </Modal.Body>
        <Modal.Footer>
          {selectedBadge.type} ({selectedBadge.size})
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Gallery;
