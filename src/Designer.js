import React, {useState, useEffect} from 'react';
import {Prompt} from 'react-router-dom';
import AceEditor from 'react-ace';
import JSZip from 'jszip';
import {saveAs} from 'file-saver';
import pngChunksExtract from 'png-chunks-extract';
import pngChunkText from 'png-chunk-text';
import pngChunksEncode from 'png-chunks-encode';

import {When} from 'react-if';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';

import 'ace-builds/src-noconflict/mode-json';
import 'ace-builds/src-noconflict/theme-github';

import Template from './Template';

const TemplateKeyword = 'crd:template';

const exampleTemplate = {
  crdVersion: 1,
  author: {
    name: 'bettse',
    gitlab: 'bettse',
    github: 'bettse',
    twitter: '@aguynamedbettse',
    email: 'bettse@fastmail.fm',
    anything: 'value',
  },
  orientation: 'portrait',
  fonts: ['https://fonts.googleapis.com/css2?family=Odibee+Sans&display=swap'],
  overlays: [
    {
      type: 'text',
      label: 'custom font',
      x: 319,
      y: 90,
      'text-anchor': 'middle',
      style: {
        'font-size': '40px',
        'font-family': 'Odibee Sans, Arial',
      },
    },
    {
      type: 'image',
      label: 'example image',
      x: 159,
      y: 260,
      width: 320,
      height: 240,
    },
    {
      type: 'qrcode',
      label: 'qrcode',
      x: 300,
      y: 650,
      width: 240,
      height: 240,
    },
    {
      type: 'text',
      label: 'text centered',
      x: 319,
      y: 572,
      'text-anchor': 'middle',
      style: {
        'text-transform': 'capitalize',
        'font-size': '72px',
      },
    },
    {
      type: 'text',
      label: 'text left',
      x: 10,
      y: 818,
      style: {
        'text-transform': 'uppercase',
        'font-size': '24px',
      },
    },
    {
      type: 'barcode',
      label: 'barcode',
      format: 'code128',
      x: 10,
      y: 930,
      width: 618,
      height: 72,
    },
  ],
};

function Designer(props) {
  const [crd, setCrd] = useState(null);
  const [background, setBackground] = useState(null);
  const [template, setTemplate] = useState(exampleTemplate);
  const [textTemplate, setTextTemplate] = useState(
    JSON.stringify(exampleTemplate, null, 4),
  );
  const [status, setStatus] = useState({variant: '', message: ''});
  const [dirty, setDirty] = useState(false);

  function fileToDataUrl(f) {
    // I wish I could do this:
    //setBackground(URL.createObjectURL(newBackground, {type: 'image/png'}));
    // But like when using a URL, the image isn't kept in the downloaded svg-to-png

    const reader = new FileReader();
    reader.readAsDataURL(f);
    reader.addEventListener(
      'load',
      () => {
        setBackground(reader.result);
      },
      false,
    );
  }

  useEffect(() => {
    const extractPng = async () => {
      try {
        fileToDataUrl(crd);

        const buffer = new Uint8Array(await crd.arrayBuffer());
        const chunks = pngChunksExtract(buffer);
        const chunk = chunks
          .filter(chunk => chunk.name === 'tEXt')
          .map(chunk => pngChunkText.decode(chunk.data))
          .find(({keyword}) => keyword === TemplateKeyword);
        if (!chunk) {
          return;
        }
        setTextTemplate(chunk.text);
      } catch (e) {
        console.log('error extracting crd:', e);
        setCrd(null);
      }
    };

    const extractZip = async () => {
      try {
        const zip = await JSZip.loadAsync(crd);
        const newBackground = await zip.file('background.png').async('blob');

        fileToDataUrl(newBackground);

        const templateJson = await zip.file('template.json').async('blob');
        const json = await templateJson.text();
        setTextTemplate(json);
      } catch (e) {
        console.log('error extracting crd:', e);
        setCrd(null);
      }
    };
    if (crd) {
      if (crd.type === 'image/png') {
        extractPng();
      } else if (crd.type === 'application/zip' || crd.type === '') {
        extractZip();
      } else {
        console.log('unhandled type', crd.type);
        setCrd(null);
      }
    }
  }, [crd]);

  async function dataUriToUint8Array(uri) {
    const response = await fetch(uri);
    const blob = await response.blob();
    const ab = await blob.arrayBuffer();
    const buffer = new Uint8Array(ab);
    return buffer;
  }

  async function download() {
    try {
      const content = JSON.stringify(template, null, 4);
      const buffer = await dataUriToUint8Array(background);

      // Remove existing template
      const chunks = pngChunksExtract(buffer).filter(chunk => {
        if (chunk.name === 'tEXt') {
          const decoded = pngChunkText.decode(chunk.data);
          if (decoded.keyword === TemplateKeyword) {
            return false;
          }
        }
        return true;
      });

      const chunk = pngChunkText.encode(TemplateKeyword, content);
      // Add new/updated template
      chunks.splice(-1, 0, chunk);

      const uint8Array = pngChunksEncode(chunks);

      const blob = new Blob([uint8Array], {type: 'image/png'});
      saveAs(blob, crd.name);
      setDirty(false);
    } catch (e) {
      console.log('error with download', e);
      setStatus({variant: 'warning', message: 'error with download'});
    }
  }

  useEffect(() => {
    if (textTemplate === JSON.stringify(template, null, 4)) {
      // Skip if it is the same (usually on load)
      return;
    }
    try {
      const t = JSON.parse(textTemplate);
      setTemplate(t);
      setStatus({variant: '', message: ''});
    } catch (e) {
      setStatus({variant: 'danger', message: 'invalid json'});
    }
  }, [textTemplate, template]);

  return (
    <>
      <Prompt
        when={dirty}
        message={location =>
          `Are you sure you want to go to ${location.pathname}`
        }
      />
      <Row>
        <Col>
          <Form.Group>
            <Form.Text>
              This supports both the legacy (zip file as 'crd') and new (png
              with template) formats. Upload a regular png to add a template to
              it.
            </Form.Text>
            <Form.File
              custom
              onChange={e => setCrd(e.target.files[0])}
              label="Card template"
            />
          </Form.Group>
        </Col>
      </Row>
      <Row>
        <Col>
          <AceEditor
            mode="json"
            theme="github"
            name="ace-editor"
            readOnly={!crd}
            value={textTemplate}
            onChange={update => {
              setTextTemplate(update);
              setDirty(true);
            }}
            editorProps={{
              $blockScrolling: true,
            }}
            setOptions={{
              useWorker: false,
            }}
          />
        </Col>
        <Col>
          <When condition={status.variant !== ''}>
            <Alert variant={status.variant}>{status.message}</Alert>
          </When>
          <div>
            <div style={{display: 'inline-block', border: '1px dashed black'}}>
              <Template
                orientation={template.orientation}
                background={background}
                fonts={template.fonts}
                overlays={template.overlays}
              />
            </div>
          </div>
        </Col>
      </Row>
      <Row className="mt-3 text-center justify-content-center">
        <Col>
          <Button
            variant={dirty ? 'info' : 'primary'}
            disabled={!crd}
            onClick={download}>
            Download
          </Button>
          <Form.Text>Saves as PNG with embedded template</Form.Text>
        </Col>
      </Row>
    </>
  );
}

export default Designer;
