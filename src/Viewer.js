import React, {useState, useEffect, useContext, useRef} from 'react';
import {useHistory} from 'react-router-dom';
import JSZip from 'jszip';
import pngChunksExtract from 'png-chunks-extract';
import pngChunkText from 'png-chunk-text';

import {If, Then, Else, When} from 'react-if';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import ProgressBar from 'react-bootstrap/ProgressBar';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';

import toImg from './svg-to-image';
import {Context} from './Provider';
import Template from './Template';

function Viewer(props) {
  const [crd, setCrd] = useState(null);
  const [background, setBackground] = useState(null);
  const [overlays, setOverlays] = useState([]);
  const [fonts, setFonts] = useState([]);
  const [orientation, setOrientation] = useState('portrait');
  const [author, setAuthor] = useState({});
  const {setBadge} = useContext(Context);
  const history = useHistory();
  const [show, setShow] = useState(false);
  const [download, setDownload] = useState(false);
  const modalBody = useRef(null);
  const svg = useRef(null);

  function fileToDataUrl(f) {
    // I wish I could do this:
    //setBackground(URL.createObjectURL(newBackground, {type: 'image/png'}));
    // But like when using a URL, the image isn't kept in the downloaded svg-to-png

    const reader = new FileReader();
    reader.readAsDataURL(f);
    reader.addEventListener(
      'load',
      () => {
        setBackground(reader.result);
      },
      false,
    );
  }

  useEffect(() => {
    const extractPng = async () => {
      try {
        const buffer = new Uint8Array(await crd.arrayBuffer());
        const chunks = pngChunksExtract(buffer);
        const template = chunks
          .filter(chunk => chunk.name === 'tEXt')
          .map(chunk => pngChunkText.decode(chunk.data))
          .find(({keyword}) => keyword === 'crd:template');
        if (!template) {
          console.log('no template in png');
          setCrd(null);
          return;
        }

        fileToDataUrl(crd);
        const json = template.text;
        try {
          const t = JSON.parse(json);
          //TODO: support other keys in the json, maybe a background image color?
          if (t.overlays) {
            setOverlays(t.overlays);
          }
          if (t.orientation) {
            setOrientation(t.orientation);
          }
          if (t.author) {
            setAuthor(t.author);
          }
          if (t.fonts) {
            setFonts(t.fonts);
          }
        } catch (e) {
          console.log('error parsing json:', e);
          setCrd(null);
        }
      } catch (e) {
        console.log('error extracting crd:', e);
        setCrd(null);
      }
    };

    const extractZip = async () => {
      try {
        const zip = await JSZip.loadAsync(crd);
        const newBackground = await zip.file('background.png').async('blob');

        fileToDataUrl(newBackground);

        const template = await zip.file('template.json').async('blob');
        const json = await template.text();
        try {
          const t = JSON.parse(json);
          //TODO: support other keys in the json, maybe a background image color?
          if (t.overlays) {
            setOverlays(t.overlays);
          }
          if (t.orientation) {
            setOrientation(t.orientation);
          }
          if (t.author) {
            setAuthor(t.author);
          }
          if (t.fonts) {
            setFonts(t.fonts);
          }
        } catch (e) {
          console.log('error parsing json:', e);
          setCrd(null);
        }
      } catch (e) {
        console.log('error extracting crd:', e);
        setCrd(null);
      }
    };
    if (crd) {
      if (crd.type === 'image/png') {
        extractPng();
      } else if (crd.type === 'application/zip' || crd.type === '') {
        extractZip();
      } else {
        console.log('unhandled type', crd.type);
        setCrd(null);
      }
    }
  }, [crd]);

  function handleChange(index) {
    // Return function to handle onChange event
    return event => {
      const {target} = event;
      const overlay = overlays[index];
      const {type} = overlay;
      const newOverlays = [...overlays];

      switch (type) {
        case 'barcode':
        case 'qrcode':
        case 'text':
          const {value} = target;
          newOverlays[index] = {
            ...overlay,
            value,
          };
          setOverlays(newOverlays);
          break;
        case 'image':
          const {files} = target;
          const [file] = files;
          const reader = new FileReader();
          reader.readAsDataURL(file);

          reader.addEventListener(
            'load',
            () => {
              newOverlays[index] = {
                ...overlay,
                value: reader.result,
              };
              setOverlays(newOverlays);
            },
            false,
          );
          break;
        default:
          console.log('no handler for', type);
      }
    };
  }

  function renderOverlay(overlay, index) {
    const {type, label, comment} = overlay;
    switch (type) {
      case 'qrcode':
      case 'barcode':
      case 'text':
        return (
          <>
            <When condition={comment && comment.length > 0}>
              <Form.Text>{comment}</Form.Text>
            </When>
            <Form.Group as={Row} controlId={`formHorizontal${overlay.label}`}>
              <Form.Label column xs="auto">
                {label}
              </Form.Label>
              <Col xs="auto">
                <Form.Control type="text" onChange={handleChange(index)} />
              </Col>
            </Form.Group>
          </>
        );
      case 'image':
        return (
          <Form.Group>
            <When condition={comment && comment.length > 0}>
              <Form.Text>{comment}</Form.Text>
            </When>
            <Form.File
              custom
              onChange={handleChange(index)}
              label={`${overlay.width}x${overlay.height} ${label}`}
            />
          </Form.Group>
        );
      default:
        return null;
    }
  }

  async function svgClick(event) {
    const {shiftKey} = event;
    setDownload(shiftKey);
    setShow(true);
  }

  // In order to prevent the svg-to-image's clone of the svg from
  // creating a flash on screen, we put it in a modal
  useEffect(() => {
    const processImg = async () => {
      try {
        var done = false;
        setTimeout(() => {
          // Close the modal if the process has comleted within a second
          if (done) {
            if (download) {
              setShow(false);
            } else {
              history.push('/');
            }
          }
        }, 1000);
        if (download) {
          await toImg(svg.current, 'card', {modal: modalBody.current});
          done = true;
        } else {
          const data = await toImg(svg.current, 'card', {
            download,
            modal: modalBody.current,
          })
            .then(imageDataUrl => fetch(imageDataUrl))
            .then(data => data.blob());
          setBadge(data);
          done = true;
        }
      } catch (e) {
        console.log('error in processImg', e);
      }
    };
    if (show && modalBody.current && svg.current) {
      processImg();
    }
  }, [show, modalBody, svg, download, setBadge, history]);

  return (
    <>
      <Row>
        <Col>
          <When condition={author.name !== undefined}>
            <Card className="mb-3">
              <Card.Header>Designed by {author.name}</Card.Header>
              <ListGroup variant="flush">
                {Object.entries(author)
                  .filter(kv => kv[0] !== 'name')
                  .map((kv, i) => {
                    return (
                      <ListGroup.Item key={i}>
                        {kv[0]}: {kv[1]}
                      </ListGroup.Item>
                    );
                  })}
              </ListGroup>
            </Card>
          </When>

          <If condition={crd === null}>
            <Then>
              <Form.Group>
                <Form.Text>
                  This supports both the legacy (zip file as 'crd') and new (png
                  with template) formats.
                </Form.Text>
                <Form.File
                  custom
                  onChange={e => setCrd(e.target.files[0])}
                  label="Card template"
                />
              </Form.Group>
            </Then>
            <Else>
              {overlays.map((overlay, i) => {
                return <div key={i}>{renderOverlay(overlay, i)}</div>;
              })}
            </Else>
          </If>
        </Col>
        <Col>
          <div>
            <div style={{display: 'inline-block', border: '1px dashed black'}}>
              <Template
                scale={0.5}
                ref={svg}
                svgClick={svgClick}
                orientation={orientation}
                background={background}
                overlays={overlays}
                fonts={fonts}
              />
            </div>
          </div>
          <p>
            Click the SVG to load into badge request form (hold shift to
            download)
          </p>
        </Col>
      </Row>
      <Modal show={show} onHide={() => setShow(false)} centered size="xl">
        <Modal.Header>
          <Modal.Title>Rendering image</Modal.Title>
        </Modal.Header>
        <Modal.Body
          ref={modalBody}
          className="text-center justify-content-center">
          <ProgressBar animated now={100} />
        </Modal.Body>
      </Modal>
    </>
  );
}

export default Viewer;
