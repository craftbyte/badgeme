import React from 'react';

import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import ClickToCopy from 'react-copy-content';

const {REACT_APP_BITCOIN_ADDRESS} = process.env;

function Footer(props) {
  return (
    <>
      <footer className="page-footer font-small mt-5">
        <Container fluid className="text-center">
          <Row noGutters>
            <Col>
              <small className="text-muted">
                Hosted with <a href="https://www.netlify.com/">Netlify</a>
              </small>
            </Col>
            <Col>
              <small className="text-muted">
                Donations (BitCoin):&nbsp;
                <ClickToCopy
                  contentToCopy={REACT_APP_BITCOIN_ADDRESS}
                  render={props => (
                    <code onClick={props.copy}>
                      {REACT_APP_BITCOIN_ADDRESS}
                    </code>
                  )}
                />
              </small>
            </Col>
            <Col>
              <a href="/x86_64/evorasterizer">x86_64 evorasterizer</a>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
}

export default Footer;
