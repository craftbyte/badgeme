const util = require('util');
const ipp = require('ipp');

const {BADGE_PRINTER_URI, NETLIFY_DEV, LASER_PRINTER_URI} = process.env;

const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

exports.handler = async function(event, context) {
  try {
    const badgePrinter = ipp.Printer(BADGE_PRINTER_URI);
    const execute = util.promisify(badgePrinter.execute.bind(badgePrinter));

    const attrs = await execute('Get-Printer-Attributes', null);
    const pat = attrs['printer-attributes-tag'];
    const state = pat['printer-state'];
    const currentTime = pat['printer-current-time'];
    const message = pat['printer-state-message'];
    const reasons = pat['printer-state-reasons'];
    if (NETLIFY_DEV) {
      //console.log(pat);
    }

    return {
      statusCode: 200,
      body: JSON.stringify({
        state,
        currentTime,
        message,
        reasons,
      }),
    };
  } catch (e) {
    console.log(e);
  }
  return failure;
};
