const util = require('util');
const fs = require('fs');
const ipp = require('ipp');
const fetch = require('node-fetch');
const PDFDocument = require('pdfkit');
const sizeOf = require('image-size');
const {InfluxDB, Point} = require('@influxdata/influxdb-client');
const sharp = require('sharp');
const Lob = require('lob');

const approvedMime = ['image/jpeg', 'image/png'];
const MAX_SIZE_BYTES = 1 * 1024 * 1024;

// portrait
const badgePx = {
  height: 1012,
  width: 638,
};

//landscape
const badgePt = {
  height: 153,
  width: 243,
};

//PostScript points.
const letter = {
  height: 792,
  width: 612,
};

const returnWindow = {
  top: 45,
  left: 36,
  width: 252,
  height: 72,
};

const addressWindow = {
  top: 162,
  left: 36,
  width: 288,
  height: 81,
};

const {
  BADGE_PRINTER_URI,
  LASER_PRINTER_URI,
  NETLIFY_DEV = false,
  KEYBASE_WEBHOOK_URL,
  NETLIFY_API_TOKEN,
  RETURN_ADDRESS,
  LOB_API_KEY,
  EMAIL_REP_KEY,
} = process.env;

const {
  INFLUXDB_V2_BUCKET,
  INFLUXDB_V2_TOKEN,
  INFLUXDB_V2_ORG,
  INFLUXDB_V2_URL,
} = process.env;

const EMAIL_REP = 'https://emailrep.io';

const writeApi = new InfluxDB({
  url: INFLUXDB_V2_URL,
  token: INFLUXDB_V2_TOKEN,
}).getWriteApi(INFLUXDB_V2_ORG, INFLUXDB_V2_BUCKET, 'ns', {
  writeFailed: console.log,
});
writeApi.useDefaultTags({dev: !!NETLIFY_DEV});

const returnAddressParts = RETURN_ADDRESS.split('|');

const {usVerifications} = Lob(LOB_API_KEY);

const success = {
  statusCode: 200,
  body: '',
};

async function getImage(url) {
  let inputImage = null;
  if (NETLIFY_DEV) {
    inputImage = fs.readFileSync(url);
  } else {
    inputImage = await fetch(url).then(res => res.buffer());
  }

  const dimensions = sizeOf(inputImage);
  let image = null;
  if (dimensions.width < dimensions.height) {
    image = await sharp(inputImage)
      .resize(badgePx.width, badgePx.height, {fit: 'inside'})
      .png()
      .toBuffer();
  } else {
    image = await sharp(inputImage)
      .resize(badgePx.height, badgePx.width, {fit: 'inside'})
      .png()
      .toBuffer();
  }
  return {
    image,
    type: 'image/png',
  };
}

async function printBadge(badge, email) {
  const {filename, url} = badge;

  const badgePrinter = ipp.Printer(BADGE_PRINTER_URI);
  const execute = util.promisify(badgePrinter.execute.bind(badgePrinter));

  try {
    const {image, type} = await getImage(url);

    const badgeRequest = {
      'operation-attributes-tag': {
        'requesting-user-name': email,
        'job-name': filename,
        'document-format': type,
      },
      'job-attributes-tag': {
        //'print-color-mode': 'monochrome',
        //media: 'om_card_54.86x86.02mm',
        //sides: 'two-sided-long-edge',
        //'print-scaling': 'auto-fit',
      },
      data: image,
    };

    console.log('badgeRequest', badgeRequest);

    if (NETLIFY_DEV) {
      fs.writeFileSync('./dev-badge.png', image);
      return;
    }

    const badgeResult = execute('Print-Job', badgeRequest);
    //console.log('badgeResult', await badgeResult);
  } catch (e) {
    console.error(e);
  }
}

async function composePdf(data) {
  const {
    name,
    address1,
    address2,
    city,
    region,
    postalcode,
    country,
    notes,
    badge,
  } = data;
  const {url} = badge;
  const {image} = await getImage(url);

  const size = 'LETTER';

  return new Promise((resolve, reject) => {
    const doc = new PDFDocument({size});

    let buffers = [];
    doc.on('error', reject); //dunno if it actually emits this
    doc.on('data', buffers.push.bind(buffers));
    doc.on('end', () => {
      const pdfData = Buffer.concat(buffers);
      resolve(pdfData);
    });

    if (NETLIFY_DEV) {
      doc.pipe(fs.createWriteStream('./dev.pdf')); // write to PDF
    }

    // Fold marks
    const leftSide = 0;
    const foldMarkWidth = 50;
    const firstMark = letter.height * (1 / 3);
    const secondMark = letter.height * (2 / 3);

    [firstMark, secondMark].forEach(mark => {
      //left
      doc
        .moveTo(leftSide, mark)
        .lineTo(foldMarkWidth, mark)
        .dash(5, {space: 10})
        .stroke();

      //right
      doc
        .moveTo(letter.width, mark)
        .lineTo(letter.width - foldMarkWidth, mark)
        .dash(5, {space: 10})
        .stroke();
    });

    // Where to place badge
    const x = letter.width / 2 - badgePt.width / 2;
    const y = letter.height / 2 - badgePt.height / 2;

    doc.save();
    const dimensions = sizeOf(image);
    if (dimensions.width < dimensions.height) {
      doc.rotate(90, {origin: [x, y]});
      // These calculations derived experimentally.  I'm sketchy on the math.
      doc.image(image, x, badgePt.height / 2, {
        fit: [badgePt.height, badgePt.width],
      });
    } else {
      doc.image(image, x, y, {fit: [badgePt.width, badgePt.height]});
    }
    doc.restore();
    doc.rect(x, y, badgePt.width, badgePt.height).stroke();

    doc.text(`Notes:\n${notes}`, 10, secondMark + 10, {
      height: firstMark - 10,
      width: letter.width - 20,
    });

    doc.addPage({size});

    const pageOffset = letter.height / 3; // Place window contents in middle third of page

    // outline of envelope return address window
    doc
      .rect(
        returnWindow.left,
        pageOffset + returnWindow.top,
        returnWindow.width,
        returnWindow.height,
      )
      .dash(5, {space: 10})
      .stroke();

    // address (lines up with envelope window)
    const margin = {
      left: 5,
      top: 5,
    };

    doc.text(
      returnAddressParts.join('\n'),
      returnWindow.left + margin.left,
      pageOffset + returnWindow.top + margin.top,
      {width: returnWindow.width, height: returnWindow.height},
    );

    // outline of envelope address window
    doc
      .rect(
        addressWindow.left,
        pageOffset + addressWindow.top,
        addressWindow.width,
        addressWindow.height,
      )
      .dash(5, {space: 10})
      .stroke();

    // address (lines up with envelope window)
    const addressParts = [];
    addressParts.push(name);
    addressParts.push(address1);
    if (address2.length > 0) {
      addressParts.push(address2);
    }
    addressParts.push(`${city}, ${region} ${postalcode}`);
    addressParts.push(country);

    doc.text(
      addressParts.join('\n'),
      addressWindow.left + margin.left,
      pageOffset + addressWindow.top + margin.top,
      {
        width: addressWindow.width,
        height: addressWindow.height,
      },
    );

    doc.end();
  });
}

async function printAddress(data) {
  const {email} = data;

  const laserPrinter = ipp.Printer(LASER_PRINTER_URI);
  const execute = util.promisify(laserPrinter.execute.bind(laserPrinter));

  const pdf = await composePdf(data);

  const addressRequest = {
    'operation-attributes-tag': {
      'requesting-user-name': email,
      'job-name': 'address',
      'document-format': 'application/pdf',
    },
    data: pdf,
  };

  console.log('addressRequest', addressRequest);
  if (NETLIFY_DEV) {
    return;
  }
  try {
    const addressResult = execute('Print-Job', addressRequest);
    // console.log('addressRequest', await addressResult);
  } catch (e) {
    console.error(e);
  }
}

async function sendToKeybase(url) {
  return fetch(KEYBASE_WEBHOOK_URL, {
    method: 'post',
    body: JSON.stringify({msg: url}),
    headers: {'Content-Type': 'application/json'},
  });
}

async function validAddress(data) {
  const {name, address1, address2, city, region, postalcode, country} = data;
  const verify = util.promisify(usVerifications.verify.bind(usVerifications));

  try {
    const result = await verify({
      recipient: name,
      primary_line: address1,
      secondary_line: address2,
      city,
      state: region,
      zip_code: postalcode,
    });
    const {deliverability, lob_confidence_score} = result;
    const {score, level} = lob_confidence_score;
    console.log(
      'lob us address verification',
      JSON.stringify({deliverability, score, level}),
    );
  } catch (e) {
    console.log('address verification issue:', e);
    return false;
  }
}

//https://github.com/sublime-security/emailrep.io
async function validateEmail(email) {
  try {
    const response = await fetch(`${EMAIL_REP}/${email}`, {
      headers: {
        Key: EMAIL_REP_KEY,
        'Content-Type': 'application/json',
        'User-Agent': 'badgesof.ericbetts.dev',
      },
    });
    const result = await response.json();
    if (response.ok) {
      const {reputation, suspicious} = result;
      if (suspicious || ['low', 'none'].includes(reputation)) {
        console.log(result);
        return false;
      }
    } else {
      const {status, reason} = result;
      if (status === 'fail' && reason === 'invalid email') {
        return false;
      } else {
        console.log('email rep error', result);
      }
    }
  } catch (e) {
    console.log('error validating email', e);
  }
  // Due to possible failures or rate limits, can't block on failure
  return true;
}

exports.handler = async function(event, context) {
  const {body: json} = event;
  const body = JSON.parse(json);
  const {payload} = body;
  const {data} = payload;
  const {country, email, badge} = data;
  const {url, type, size} = badge;

  const point = new Point('request')
    .tag('country', country)
    .tag('type', type)
    .intField('size', size)
    .intField('value', 1);

  const now = new Date();
  const maintenance =
    NODE_ENV !== 'development' &&
    ((now.getUTCDay() === 6 && now.getUTCHours() >= 18) ||
      (now.getUTCDay() === 0 && now.getUTCHours() < 6));

  if (maintenance) {
    throw 'Submission during maintenance window';
    return success;
  }

  try {
    if (size > MAX_SIZE_BYTES) {
      point.tag('success', false);
      throw `Ignoring file of ${size /
        1024} kbytes over max of ${MAX_SIZE_BYTES}`;
      return success;
    }

    if (!approvedMime.includes(type)) {
      point.tag('success', false);
      throw `Ignoring mime type ${type}`;
      return success;
    }

    // Avoid burning rate limit during development
    if (NETLIFY_DEV) {
      console.log('skip address validation');
    } else {
      // Can't verify non-us with this API
      if (
        country &&
        (country.toUpperCase() === '' ||
          country.toUpperCase() === 'USA' ||
          country.toUpperCase() === 'UNITED STATES')
      ) {
        // For not just pass the data so the function can log.
        await validAddress(data);
      }

      if (!(await validateEmail(email))) {
        throw `Ignoring unreputable email ${email}`;
        return success;
      }
    }

    await printAddress(data);
    await printBadge(badge, email);
    point.tag('success', true);
    if (NETLIFY_DEV) {
      return success;
    }
    await sendToKeybase(url);
  } catch (e) {
    console.error(e);
    point.tag('success', false);
  } finally {
    await writeApi.writePoint(point);
    //await writeApi.close();
    await writeApi.flush();
  }

  return success;
};
