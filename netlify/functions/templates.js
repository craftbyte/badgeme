const NetlifyAPI = require('netlify');

const {NETLIFY_API_TOKEN, TEMPLATE_FORM_ID} = process.env;

const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

exports.handler = async function(event, context) {
  const form_id = TEMPLATE_FORM_ID;
  try {
    const client = new NetlifyAPI(NETLIFY_API_TOKEN);
    const submissions = await client.listFormSubmissions({form_id});
    const templates = submissions.map(submission => {
      const {data} = submission;
      const {name, template} = data;
      const {url} = template;
      return {name, url};
    });

    return {
      statusCode: 200,
      body: JSON.stringify({
        templates,
      }),
    };
  } catch (e) {
    console.log(e);
  }
  return failure;
};
