const NetlifyAPI = require('netlify');

const {NETLIFY_API_TOKEN, REQUEST_FORM_ID} = process.env;

const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

exports.handler = async function(event, context) {
  try {
    const client = new NetlifyAPI(NETLIFY_API_TOKEN);
    const submissions = await client.listFormSubmissions({
      form_id: REQUEST_FORM_ID,
    });
    const badges = submissions
      .filter(submission => {
        const {data} = submission;
        const {gallery} = data;
        return 'on' === gallery;
      })
      .map(submission => {
        const {data} = submission;
        const {badge} = data;
        return badge;
      });

    return {
      statusCode: 200,
      body: JSON.stringify({
        badges,
      }),
    };
  } catch (e) {
    console.log(e);
  }
  return failure;
};
