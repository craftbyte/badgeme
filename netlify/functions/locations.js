const NetlifyAPI = require('netlify');
const {getCode} = require('country-list');

const {NETLIFY_API_TOKEN, REQUEST_FORM_ID} = process.env;

const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

// country-list doesn't match some
const countryRename = {
  USA: 'United States of America',
  'United States': 'United States of America',
  'United Kingdom': 'United Kingdom of Great Britain and Northern Ireland',
};

exports.handler = async function(event, context) {
  try {
    const client = new NetlifyAPI(NETLIFY_API_TOKEN);
    const submissions = await client.listFormSubmissions({
      form_id: REQUEST_FORM_ID,
    });
    const countries = {};
    submissions.forEach(submission => {
      const {data} = submission;
      const {country} = data;
      const code = getCode(countryRename[country] || country);
      if (!countries[code]) {
        countries[code] = 0;
      }
      countries[code] = countries[code] + 1;
    });

    return {
      statusCode: 200,
      body: JSON.stringify({
        countries,
      }),
    };
  } catch (e) {
    console.log(e);
  }
  return failure;
};
